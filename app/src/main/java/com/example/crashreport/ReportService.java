package com.example.crashreport;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.crashreport.retrofit.APIService;
import com.example.crashreport.retrofit.APIUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e( "onStartCommand: ", "yes");
        APIService apiService = APIUtils.getAPIService(MyApplication.getContext());
        apiService.crashReport(intent.getExtras().getString("error")).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        Log.e( "onResponse: ", response.body());
                    }else{
                        Log.e( "onResponse: ", "null");
                    }
                }else{
                    Log.e( "onResponse: ", "not success");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e( "onResponse: ", "onFailure "+t.getMessage());
            }
        });

        return START_STICKY;
    }
}
