package com.example.crashreport;

import android.app.Application;

import com.balsikandar.crashreporter.CrashReporter;

public class CrashReporterSampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            //initialise reporter with external path
            CrashReporter.initialize(this);
        }
    }
}