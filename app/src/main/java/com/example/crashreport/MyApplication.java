package com.example.crashreport;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.crashreport.retrofit.APIService;
import com.example.crashreport.retrofit.APIUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyApplication extends Application {

    private static Context context;

    public static Context getContext() {
        return context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        appInitialization();
    }

    private void appInitialization() {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
    }

    private Thread.UncaughtExceptionHandler defaultUEH;

    // handler listener
    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
//            ex.printStackTrace();
            String error = "";

            for (int i = 0; i < ex.getStackTrace().length; i++) {
//                Log.e( "uncaughtException: "+i,ex.getStackTrace()[i].toString() );
                error += ex.getStackTrace()[i].toString();
            }

            APIService apiService = APIUtils.getAPIService(MyApplication.getContext());
            apiService.crashReport(error).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.e("onResponse: ", response.body());
                        } else {
                            Log.e("onResponse: ", "null");
                        }
                    } else {
                        Log.e("onResponse: ", "not success");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("onResponse: ", "onFailure " + t.getMessage());
                }
            });

         /*   Intent intent = new Intent(MyApplication.this,ReportService.class);
            intent.putExtra("error",error);
            startService(intent);
*/
        }
    };
}
