package com.example.crashreport.retrofit;

import android.content.Context;

public class APIUtils {
    private APIUtils() {
    }

    public static String url = "http://172.20.10.5";

    public static APIService getAPIService(Context context) {
        return RetrofitClient.getClient(url+"/crash/").create(APIService.class);
    }
}
