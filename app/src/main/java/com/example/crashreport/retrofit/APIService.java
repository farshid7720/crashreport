package com.example.crashreport.retrofit;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("crash_report.php")
    Call<String> crashReport(@Field("error") String error);

}
