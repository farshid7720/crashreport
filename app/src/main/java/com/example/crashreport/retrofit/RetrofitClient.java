package com.example.crashreport.retrofit;

import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
  private static Retrofit retrofit = null;

  public static Retrofit getClient(String baseUrl) {
    if (retrofit==null) {
      Gson gson = new GsonBuilder()
              .setLenient()
              .create();
      final OkHttpClient okHttpClient = new OkHttpClient.Builder()
              .readTimeout(60, TimeUnit.SECONDS)
              .connectTimeout(60, TimeUnit.SECONDS)
              .build();

      retrofit = new Retrofit.Builder()
              .baseUrl(baseUrl)
              .addConverterFactory(GsonConverterFactory.create(gson))
              .client(okHttpClient)
              .build();
    }

    return retrofit;
  }


}
